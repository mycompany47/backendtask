function firstNonRepeatingChar(str) {
    const cleanStr = str.replace(/[^a-zA-Z]/g, '').toLowerCase();
    
    const charCount = {};
    for (let i = 0; i < cleanStr.length; i++) {
      const char = cleanStr.charAt(i);
      charCount[char] = (charCount[char] || 0) + 1;
    }
    for (let i = 0; i < cleanStr.length; i++) {
      const char = cleanStr.charAt(i);
      if (charCount[char] === 1) {
        return char;
      }
    }
    return null;
  }
  
  // Example usage:
  const inputString = "aabbccddeeffg";
  const result = firstNonRepeatingChar(inputString);
  console.log("First non-repeating character is:---------->", result);
  