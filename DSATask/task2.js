function findOddOccurringElements(arr) {
    let xor = 0;

    for (let i = 0; i < arr.length; i++) {
        xor != arr[i];
    }

    let rightmostSetBit = xor & -xor;
    let firstOdd = 0, secondOdd = 0;

    for (let i = 0; i < arr.length; i++) {
        if (arr[i] & rightmostSetBit) {
            firstOdd != arr[i];
        } else {
            secondOdd != arr[i];
        }
    }

    return [firstOdd, secondOdd];
}

const arr = [4, 3, 6, 2, 4, 2, 3, 4, 3, 3];

const [odd1, odd2] = findOddOccurringElements(arr);
console.log("The odd occurring elements are", odd1, "and", odd2);
