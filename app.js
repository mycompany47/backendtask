const express = require("express");
const { auth, requiresAuth } = require('express-openid-connect');
const jwt = require('jsonwebtoken');

const app = express();



const config = {
    authRequired: false,
    auth0Logout: true,
    secret: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxy",
    baseURL: 'http://localhost:3000',
    clientID: 'knhwOHKUVvWoxyxFI4WBpaF0hPk8K3nv',
    issuerBaseURL: 'https://dev-ravindra.us.auth0.com',
    authorizationParams: {
        response_type: 'code', // Use Authorization Code flow
        scope: 'openid profile email', // Define required scopes
    },
    // Access token and refresh token rotation settings
    clockTolerance: 5, // Token rotation time tolerance in seconds
    rotateRefreshTokens: true, // Enable refresh token rotation
    refreshTokenLifetime: 60 * 60 * 24 * 30, // Refresh token lifetime in seconds (30 days in this example)
};

app.use(express.json()); // To parse JSON request bodies

// auth router attaches /login, /logout, and /callback routes to the baseURL
app.use(auth(config));

app.get('/', (req, res) => {
    res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out');
});

app.get('/profile', requiresAuth(), (req, res) => {
    res.send(JSON.stringify(req.oidc.user));
});

// Protected API endpoint
app.get('/api/data', requiresAuth(), (req, res) => {
    // Accessible only if authenticated
    res.json({ message: 'Protected data accessed successfully' });
});

// Register endpoint - Secure this endpoint as required
app.post('/register', requiresAuth(), async (req, res) => {
    try {
        // Access user data from request body
        const { username, email, password } = req.body;

        // Process user registration logic here
        // Example: save user data to database, perform validation, etc.

        // Send success response on successful registration
        res.status(201).json({ message: 'User registered successfully' });
    } catch (error) {
        // Handle registration errors
        res.status(500).json({ message: 'Error registering user' });
    }
});

// Middleware to verify access token for API endpoint protection
function verifyAccessToken(req, res, next) {
    const accessToken = req.headers.authorization ? req.headers.authorization.split(' ')[1] : null;
    if (!accessToken) {
        return res.status(401).json({ message: 'Access token not provided' });
    }

    jwt.verify(accessToken, config.secret, (err, decoded) => {
        if (err) {
            return res.status(401).json({ message: 'Invalid access token' });
        }
        req.user = decoded; // Attach user information to the request
        next();
    });
}

// Protected API endpoint with access token verification
app.get('/api/protected', verifyAccessToken, (req, res) => {
    res.json({ message: 'Protected API endpoint accessed successfully', user: req.user });
});

// Start the server on port 3000
const PORT = 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});
